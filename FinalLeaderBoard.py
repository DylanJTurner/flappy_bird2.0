from tkinter import *
from tkinter.font import *
import json

bg = 'orange'
fg = 'black'

number_of_people = 5


def load_data():
    with open("scores.json") as file:
        return json.load(file)["scores"]


def sort_data():
    data = load_data()
    data.sort(key=lambda d: d['score'], reverse=True)
    return data


# This sets up the dimensions of screen, name of screen and colour of screen.
def show_leaderboard(parent_window_function):
    root = Tk()
    root.geometry("500x750")
    root.title("LeaderBoard")
    root.resizable(0, 0)
    root.configure(bg='blue')

    def go_back_to_menu():
        root.destroy()

        parent_window_function()

    # Go back to main menu button, also fonts, size and position on screen
    go_back_button = Button(root, text="Go back to menu", font=Font(size=12), bg=bg, foreground=fg,
                            command=go_back_to_menu)
    go_back_button.place(relx=0.01, rely=0.01)

    header_label = Label(root, text="Leaderboard", bg=bg, foreground=fg, font=Font(size=15))
    header_label.place(relx=0.5, anchor=CENTER, rely=0.15)

    scores = sort_data()
    # This checks if there are any scores
    if len(scores) == 0:
        label = Label(root, bg=bg, foreground=fg, text="No scores yet")
        label.place(rely=0.4, relx=0.5, anchor=CENTER)
    # checks to see if score is greater than 1 and displays it on screen
    elif len(scores) <= number_of_people:
        rely_top = 0.25
        increment = 0.1
        for i in scores:
            text = i["name"] + " got a score of " + str(i["score"])

            label2 = Label(root, bg=bg, foreground=fg, text=text)
            label2.place(rely=rely_top, relx=0.5, anchor=CENTER)

            rely_top += increment
    # checks to see how many scores are recorded and orders top 5 from best to worst
    else:
        rely_top = 0.25
        increment = 0.1
        for i in range(number_of_people):
            text = scores[i]["name"] + " got a score of " + str(scores[i]["score"])

            label2 = Label(root, bg=bg, foreground=fg, text=text)
            label2.place(rely=rely_top, relx=0.5, anchor=CENTER)

            rely_top += increment

    root.mainloop()


if __name__ == '__main__':
    show_leaderboard(1)
