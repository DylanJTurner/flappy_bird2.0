from tkinter import *
from FinalAddScore import *
from FinalLeaderBoard import *
import FinalGame

# colours for background and foreground text
bg = 'orange'
fg = 'black'


# sets dimensions for screen, size of screen, name of screen and background
def run_window():
    root = Tk()
    root.title("Main Menu")
    root.geometry("500x750")
    root.resizable(0, 0)
    root.configure(bg='blue')

    def show_score_window():
        root.destroy()

        add_score_window(run_window)

    # function that when start is clicked prints button clicked and runs the game function
    def onclick():
        root.destroy()
        print("Button Clicked")
        FinalGame.flappy()

    # function for leaderboard, destroys window and runs leaderboard function
    def show_leader_board_window():
        root.destroy()
        show_leaderboard(run_window)

    # labels for start, leaderboard and addscore screen.
    # assigns the labels commands such as positioning on screen, colour, width and height.
    label = Label(root, text="The Ninja Game", bg=bg, foreground=fg, width=66, height=6, )
    label.place(anchor=CENTER, rely=0.08, relx=0.5)

    start_button = Button(root, text="Start", bg=bg, foreground=fg, width=66, height=4, command=onclick)
    start_button.place(anchor=CENTER, rely=0.55, relx=0.5)

    leaderboard_button = Button(root, text="Leaderboard", bg=bg, foreground=fg, width=30, height=4,
                                command=show_leader_board_window)
    leaderboard_button.place(anchor=CENTER, rely=0.7, relx=0.75)

    designed_label = Label(root, text="Designed by Dylan", bg=bg, foreground=fg, width=66, height=4, )
    designed_label.place(anchor=CENTER, rely=0.85, relx=0.5)

    add_score_button = Button(root, text="Add Score", bg=bg, foreground=fg, width=30, height=4,
                              command=show_score_window)
    add_score_button.place(anchor=CENTER, rely=0.7, relx=0.25)

    # leaderBoardButton = Button(root, text="Leaderboard", command=show_leader_board_window)
    # leaderBoardButton.place(rely=0.6, anchor=CENTER, relx=0.5)

    root.mainloop()


if __name__ == '__main__':
    run_window()
