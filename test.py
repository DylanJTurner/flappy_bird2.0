from tkinter import *
import tkinter.font as font

window = Tk()


def onclick():
    print("Button Clicked")


main_font = font.Font(size=20)
play_font = font.Font(size=40)
width = 500
height = 750
window.geometry(f"{width}x{height}")
window.resizable(0, 0)

title = "flappy bird"
window.title(title)

window.configure(bg='blue')

bg = 'orange'
fg = 'black'
label = Label(window, text="Flappy Bird Game", bg=bg, foreground=fg, width=30, height=4, )
label.place(anchor=CENTER, rely=0.06, relx=0.5)

playbutton = Button(window, text=" My game", bg=bg, foreground=fg, command=onclick)
playbutton.place(anchor=CENTER, rely=0.2, relx=0.5)

startbutton = Button(window, text="Start", bg=bg, foreground=fg, width=30, height=4, command=onclick)
startbutton.place(anchor=CENTER, rely=0.55, relx=0.5)

leaderbordbutton = Button(window, text="Leaderboard", bg=bg, foreground=fg, width=30, height=4, command=onclick)
leaderbordbutton.place(anchor=CENTER, rely=0.7, relx=0.5)

designedlabel = Label(window, text="Designed by Dylan", bg=bg, foreground=fg, width=30, height=4, )
designedlabel.place(anchor=CENTER, rely=0.85, relx=0.5)

window.mainloop()
