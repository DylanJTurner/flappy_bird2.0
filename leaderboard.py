from tkinter import *
import tkinter.font as font

window = Tk()

def onclick():
    print ("Button Clicked")

mainFont = font.Font(size=20)
playFont = font.Font(size=40)
width = 500
height = 750
window.geometry(f"{width}x{height}")
window.resizable(0, 0)

title = "flappy bird"
window.title(title)

window.configure(bg='blue')

bg = 'orange'
fg = 'black'
label = Label(window, text="Leaderboard", bg=bg, foreground=fg, width=30, height=4, )
label.place(anchor=CENTER, rely=0.06, relx=0.5)

namelabel = Label(window, text="Name", bg=bg, foreground=fg, width=30, height=4, )
namelabel.place(anchor=CENTER, rely=0.2, relx=0.25)

scorelabel = Label(window, text="Score", bg=bg, foreground=fg, width=30, height=4,)
scorelabel.place(anchor=CENTER, rely=0.2,relx=0.75)

homebutton = Button(window, text=" Home Screen", bg=bg, foreground=fg, command=onclick)
homebutton.place(anchor=CENTER, rely=0.05, relx=0.12)
#
# startbutton = Button(window, text="Start", bg=bg, foreground=fg, width=30, height=4, command=onclick)
# startbutton.place(anchor=CENTER, rely=0.55, relx=0.5)
#
# leaderbordbutton = Button(window, text="Leaderboard", bg=bg, foreground=fg, width=30, height=4, command=onclick)
# leaderbordbutton.place(anchor=CENTER, rely=0.7, relx=0.5)
#

window.mainloop()
