from tkinter import *
from tkinter.font import Font
import json

bg = 'orange'
fg = 'black'

def add_score_window(parent_window_func):
    root = Tk()
    root.title("Add Scores")
    root.geometry("500x750")
    root.resizable(0, 0)
    root.configure(bg='blue')
    def go_back_to_menu():
        root.destroy()

        parent_window_func()

    go_back_button = Button(root, text="Go back to menu", font=Font(size=12),bg=bg, foreground=fg, command=go_back_to_menu)
    go_back_button.place(relx=0.01, rely=0.01)

    name_label = Label(root, text="Name:", font=Font(size=12), bg=bg, foreground=fg)
    name_entry = Entry(root, font=Font(size=12))

    name_label.place(relx=0.5, anchor=CENTER, rely=0.15)
    name_entry.place(relx=0.5, anchor=CENTER, rely=0.2)

    score_label = Label(root, text="Score:", bg=bg, foreground=fg)
    score_entry = Entry(root)

    score_label.place(relx=0.5, anchor=CENTER, rely=0.3)
    score_entry.place(relx=0.5, anchor=CENTER, rely=0.35)

    def add_score_to_button(name: str, score: str):
        if not score.isdigit() or len(name) < 1:
            return



        with open("scores.json") as file:
            data = json.load(file)

        player_data = {
            "name": name,
            "score": int(score)
        }
        data["scores"].append(player_data)

        with open("scores.json", "w") as file:
            json.dump(data, file)

    add_score_button = Button(root, text="Add Score to File", font=Font(size=12),bg=bg, foreground=fg,
                            command=lambda: add_score_to_button(name_entry.get(), score_entry.get()))
    add_score_button.place(relx=0.5, anchor=CENTER, rely=0.8)

    root.mainloop()


if __name__ == '__main__':
    add_score_window()
